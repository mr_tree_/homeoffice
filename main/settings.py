# Time schedule, at which times the soundfile is played.
# Please note that the elements in list should be in the chronological order.
# The last element signals the end timestamp. After that, no more ringing will occur.
SCHEDULE = [
    "07:00",
    "09:30",
    "09:45",
    "12:30",
    "13:00",
    "16:00",
]

# Drive letters and path for mapping. Please note that UNC path
# has double time \ characters.
DRIVES = {
    "B": "\\\\obelix\\baum",
    "H": "\\\\obelix\\alle",
    "J": "\\\\obelix\\meko-td\\kunden"
}

# Times the sound is played
REPEAT = 1

# Soundfile which is played at reaching the time marks from schedule.
# ! Important !: Has to be a .wav file.
SOUNDFILE = "../sounds/gong_1.wav"
