""" This module plays a sound file if the time from the schedule is reached.

Author:         Patrick Baum
Created:        2020-09-09
Last updated:   2022-06-30
"""
import os
import subprocess
import time
from datetime import datetime
from pygame import mixer
from settings import SCHEDULE, REPEAT, SOUNDFILE, DRIVES


def map_drives():
    """ This method will map all drives to the related pathes. """
    print("Let's map some drives: ")
    cmd = "C:/Windows/System32/net.exe use {}: {}"
    for drive, path in DRIVES.items():
        print("  > net use {}: {} /persistent:no .. ".format(path, drive), end="")
        try:
            with open(os.devnull, "wb") as devnull:
                subprocess.check_call(
                    cmd.format(drive, path),
                    stdout=devnull,
                    stderr=subprocess.STDOUT,
                )
            print("done")
        except subprocess.CalledProcessError:
            print("failed")


def schedule_watchdog():
    """ This method watchs if actual time is in time schedule. """
    mixer.init()
    mixer.music.load(SOUNDFILE)
    print("Start watchdog for time schedule ..")
    while True:
        timestamp = datetime.now().strftime("%H:%M")
        if timestamp in SCHEDULE:
            print("  > Time from schedule has reached: {}".format(timestamp))
            for _ in range(REPEAT):
                mixer.music.play()
            if timestamp != SCHEDULE[-1]:
                time.sleep(60)
            else:
                print("Last timestamp reached!")
                break
        else:
            time.sleep(10)


if __name__ == "__main__":
    print("Another day in homeoffice ..")
    try:
        # Map drives
        map_drives()

        # Start watchdog for time schedule
        schedule_watchdog()
    except (KeyboardInterrupt, SystemExit):
        print("Interrupted by User. What's wrong?")

    print("Done for today. See you next time :-)")
