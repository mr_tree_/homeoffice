# homeoffice #

This script should be a little help if you work from home.

<!-- Description -->
## Description: ##

It offers the possibility to map some network drives if you have a remote connection to your companies network. For that, you only have
to add all relevant network letters and paths to the DRIVES list in settings.py.

If done so, the script will try to map those paths to the drive letters. If anything worked allright, it is marked up with done. Otherwise, there
was an error. Most often, the drive letters are already mapped.

It also runs a task to watch time. If the actual time passes an entry from SCHEDULE (also in settings.py stored), it will play a .wav-file, stored in the directory **sounds**. You can add more customized sound files (also in .wav format).

The last element from SCHEDULE is used at closing time. So it is important to place the ending time at the end of the list. Otherwise the script will end with time watching at a earlier date.

<!-- Prerequesites -->
## Prerequisites: ## 
For probably running, you should to install Python 3.9 (maybe also lower versions, but it was developed with Python 3.9). 
You can get your Python download here: [Python 3.9](https://www.python.org/downloads/)

You also need to install the third party libraries from **requirements.txt**. The easiest way is to do this with pip by running `pip install -r path/to/requirements.txt`.

<!-- Usage -->
## Usage ##

For starting the script, you just have to start app.py in main directory. You can do this manually by typing it in the command line or you use the template .bat file. In this file, there are to options:
* Run with python.exe -> Output is showed on console
* Run with pythonw.exe -> Script runs in background, no console is showed

You can add your own specifications to settings.py. There are the lists which inherit the time schedule, the drives and the name of the sound file.

<!-- Contact -->
## Contact ##

Patrick Baum - baum_patrick@web.de

Project Link: [https://gitlab.com/mr_tree_/homeoffice.git](https://gitlab.com/mr_tree_/homeoffice.git)
